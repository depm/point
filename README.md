﻿# Point Catalogo en Linea
![]([https://bitbucket.org/depm/point/raw/4e92018205a94dfbf1a9f8b381fffd51c8ec66a0/src/main/resources/static/images/contacto.png](https://bitbucket.org/depm/point/raw/4e92018205a94dfbf1a9f8b381fffd51c8ec66a0/src/main/resources/static/images/contacto.png)) 
Durante este proyecto, se desarrollo una aplicación web, la cual cumple con el objetivo de ser un catalogo en linea para una tienda.
Contando con las secciones de:

### Pagina principal
 - Home
 - Catalogo
 - Conocenos
 - Contacto

### Administradores
 - Dashboard
 - Crear producto
 - Eliminar Producto

# Características
- **Proyecto inicializado en:** [Spring Initializr](https://start.spring.io/)
- **Versión de Spring**  2.2.5.RELEASE
- **Lenguaje:** Java
- **Proyecto:** Maven Project
- **Packing:** Jar
- **Vistas:** jsp
- **Base de Datos:** SQL v5.7.28
- **IDE:** Visual Studio Code

## Dependencias Utilizadas

 - Spring Boot starter data jpa
 - Apache Tomcat embed
 - Mysql connector java v8.0.194
 - Spring Boot starter security
 - java servlet jstl

## Propiedades de la aplicación

En el archivo [aplication.properties](https://bitbucket.org/depm/point/src/master/src/main/resources/application.properties.template) se encuentran las propiedades que debe tener la aplicación. 
Este archivo contiene:

 - El puerto en el que esta siendo ejecutado
 - La configuración para las vistas de jsp
 - La configuración para el consumo de la base de datos

## Descargar, compilar y ejecutar
### Descargar - Clonar

Para descargar el archivo tienes dos opciones la primera es directamente desde la descarga de los [tags](https://bitbucket.org/depm/point/downloads/?tab=tags) y otra es desde la linea de comandos de git bash con el comando:

    git clone https://bitbucket.org/depm/point.git
    
### Compilar y ejecutar
La manera mas sencilla de compilar y ejecutar tu proyecto es instalando el plugin [Spring Boot Dashboard](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-spring-boot-dashboard) dentro de Visual Studio Code, el cual te permitirá inicializar el proyecto con un solo clic
![](https://raw.githubusercontent.com/Microsoft/vscode-spring-boot-dashboard/master/images/boot-dashboard-vsc.gif)


## Branches
Durante el desarrollo se implemento el uso de git flow utilizando los siguientes comandos:
Inicializar el flujo 

    git flow init

Para abrir un nuevo branch

    git flow feature start (MYFEATURE)

Para cambiar entre branches 

	git flow checkout (MYFEATURE)
Para publicar una caracteristica

	git flow feature publish MYFEATURE

Para terminar un branch

	git flow feature finish (MYFEATURE)

Para traer los cambio desde el repositorio

	git flow feature pull origin (MYFEATURE)

Para hacer un release

	git flow release start 1.0.0
	git flow release finish 1.0.0

Para publicar el tag

	 git push origin --tags




