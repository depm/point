<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>

    <jsp:attribute name="title">
        <title>Point | Catalogo</title>
    </jsp:attribute>
    <jsp:body>
        <section class="tiles">
        <c:forEach items="${productos}" var="productos">
            <article class="style6">
                <span class="image">
                    <img src="${productos.getImageUrl()}" alt="" />
                </span>
                <a href="/catalogo/${productos.getId()}">
                    <h2>${productos.getNomPro()}</h2>
                </a>
            </article>
        </c:forEach>
        </section>
    </jsp:body>
</t:main>