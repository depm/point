<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>
    <jsp:attribute name="title">
        <title>Point | ${producto.getNomPro()}</title>
    </jsp:attribute>
    <jsp:body>
        <section class="row">
            <div class="col-5"><span class="image fit padding"><img src="${producto.getImageUrl()}" alt="" /></span></div>
            <div class="col-6 col-12-medium">
                <header>
                    <h1>${producto.getNomPro()}</h1>
                </header>
                <div>
                    <p><strong>Modelo: </strong> ${producto.getModelo()}</p>
                    <p><strong>Talla: </strong> ${producto.getTallas()}</p>
                    <p><strong>Genero: </strong> ${producto.getGenero()}</p>
                    <blockquote>${producto.getDescripcion()}</blockquote>
                </div>
                <a href="https://api.whatsapp.com/send?phone=528114754666&text=Hola,%20me%20gustaria%20mas%20informacion%20sobre%20${producto.getNomPro()}" class="button fit">Adquiere este producto</a>
            </div>
        </section>
    </jsp:body>
</t:main>