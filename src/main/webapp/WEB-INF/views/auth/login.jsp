<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:dashboard>

    <jsp:attribute name="title">
        <title>Admin | Login</title>
    </jsp:attribute>
    <jsp:body>
        <h2>iniciar sesion</h2>

        <c:if test="${errorMessage != null && !errorMessage.isEmpty()}">
            <div class="alert alert-danger ">
                ${errorMessage}
            </div>
        </c:if>
        
            <form method="post" class="form">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

                <div class="form-group">
                    <label for="username">Username:</label>
                    <input type="text" name="username"
                        id="username" class="form-control">
                </div>

                <div class="form-group">
                    <label for="password">Contraseña:</label>
                    <input type="password" name="password"
                        id="password" class="form-control">
                </div>

                <input type="submit" class="btn btn-primary btn-block" value="Iniciar sesión">
            </form>

        
    </jsp:body>
    </t:dashboard>

            