<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>
    <jsp:attribute name="title">
        <title>PointMty</title>
    </jsp:attribute>
<jsp:body>

							<header>
								<h1>Manten siempre <br />
								Your style on Point </h1>
							</header>
							<div class="box alt">
								<div class="row gtr-uniform">
									<div class="col-12"><span class="image fit"><img src="https://dl.dropboxusercontent.com/s/9hrltqldon49rqq/home.png?dl=0" alt="" /></span>
									</div></div>

									<h6>Nuestros Diseños</h6>
							
							<section class="tiles">
								<article class="style1">
									<span class="image">
										<img src="/images/pic01.jpg" alt="" />
									</span>
									<a href="/catalogo/1">
										<h2>Nocturnal Thoughts Hombre</h2>
										<div class="content">
											<p>Porque los hombres tambien pensamos por las noches</p>
										</div>
									</a>
								</article>
								<article class="style2">
									<span class="image">
										<img src="/images/pic02.jpg" alt="" />
									</span>
									<a href="/catalogo/4">
										<h2>Rainbow Cloud Mujer</h2>
										<div class="content">
											<p>Una pequeña nuve de colores</p>
										</div>
									</a>
								</article>
								<article class="style3">
									<span class="image">
										<img src="/images/Rainbow.jpg" alt="" />
									</span>
									<a href="/catalogo/3">
										<h2>Rainbow Cloud Hombre</h2>
										<div class="content">
											<p>Los arcoiris tambien son para hombres</p>
										</div>
									</a>
								</article>
							</section>
	</jsp:body>
</t:main>
						