<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:dashboard>
    <jsp:attribute name="title">
        <title>Admin | Dashboard</title>
    </jsp:attribute>
<jsp:body>

    <div class="row">
        <div class="col-6 col-12-medium">
            <ul class="actions">
                <li><a href="/admin/dashboard/crear" class="button ">Agregar nuevo producto</a></li>
                <li><a href="/" class="button primary ">Volver a Home</a></li>
                <li>
                    <form action="/auth/logout" method="post">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <input type="submit" class="button primary" value="Cerrar sesión">
                    </form>
                </li>
            </ul>
        </div>
        </div>
    <div class="col-12">
        <c:if test="${successMessage != null && !successMessage.isEmpty()}">
            <div class="alert alert-success ">
                ${successMessage}
            </div>
        </c:if>
    </div>
<div class="col-12">
    <div class="table-wrapper">
        <h2>Productos</h2>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Modelo</th>
                    <th>Talla</th>
                    <th>Genero</th>
                    <th>Disp.</th>
                    <th>Accion</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${productos}" var="productos">
                    <tr>
                        <th>
                            ${productos.getId()}
                        </th>
                        <th>
                            <em>${productos.getNomPro()}</em>
                        </th>
                        <th>
                            <em>${productos.getModelo()}</em>
                        </th>
                        <th>
                            <em>${productos.getTallas()}</em>
                        </th>
                        <th>
                            <em>${productos.getGenero()}</em>
                        </th>
                        <th class="none"> 
                            <em>${productos.getDisponibilidad()}</em>
                        </th>
                        <th>
                            <a href="/admin/dashboard/${productos.getId()}" class="button small">Editar</a>

                            <form action="/admin/dashboard/${productos.getId()}/delete" method="post" >
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                <button class="button primary small">Eliminar</a>
                            </form>
                        </th>
                    </tr>
                </c:forEach>
            </tbody>
            
        </table>
    </div>
</div>
<div class="col-12">
    <div class="table-wrapper">
        <h2>Comentarios</h2>
        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Apellido</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Comentario</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${contacto}" var="contacto">
                    <tr>
                        <th>
                            ${contacto.getId()}
                        </th>
                        <th>
                            <em>${contacto.getApellido()}</em>
                        </th>
                        <th>
                            <em>${contacto.getNombre()}</em>
                        </th>
                        <th>
                            <em>${contacto.getCorreo()}</em>
                        </th>
                        <th>
                            <em>${contacto.getComentario()}</em>
                        </th>
                    </tr>
                </c:forEach>
            </tbody>
            
        </table>
    </div>
</div>
    


</jsp:body>
</t:dashboard>