<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:dashboard>
    <jsp:attribute name="title">
        <title>Admin | Editar producto</title>
    </jsp:attribute>
    <jsp:body>
        <h2>Editar Poducto</h2>

        <c:if test="${successMessage != null && !successMessage.isEmpty()}">
            <div class="alert alert-success ">
                ${successMessage}
            </div>
        </c:if>
        <spring:hasBindErrors name="productoForm">
            <div class="alert alert-danger ">
               <ul >
                   <c:forEach items="${errors.allErrors}" var="error">
                        <li>${error.defaultMessage}</li>
                </c:forEach>
               </ul>
            </div>
        </spring:hasBindErrors>

        <div class = "noshow"> 
            <c:if test="${producto.hasTallas('S') == true}">
                    ${TallaS = "checked"}
            </c:if>
    
            <c:if test="${producto.hasTallas('M') == true}">
                    ${TallaM = "checked"}
            </c:if>
    
            <c:if test="${producto.hasTallas('G') == true}">
                    ${TallaG = "checked"}
            </c:if>
    
            <c:if test="${producto.hasGenero('Hombre') == true}">
                    ${Hombre = "checked"}
            </c:if>
    
            <c:if test="${producto.hasGenero('Mujer') == true}">
                    ${Mujer = "checked"}
            </c:if>
    
            <c:if test="${producto.hasModelo('V') == true}">
                    ${CuelloV = "checked"}
            </c:if>
    
            <c:if test="${producto.hasModelo('Redondo') == true}">
                    ${CuelloR = "checked"}
            </c:if>
        </div>

        <div class="col-6 col-12-xsmall">
        Id del producto:  <span ><strong>${producto.getId()}</strong></span>        </div>
            <form method="post" action="/admin/dashboard/${producto.getId()}">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <div class="row gtr-uniform">
                <div class="col-6 col-12-xsmall">
                    <input type="text" name="nom_pro" id="nom_pro" value=" ${ producto.getNomPro()}" 
                    placeholder="Nombre del Producto" /> 
                </div>
                <div class="col-6 col-12-xsmall">
                    <input type="number" name="disponibilidad" id="disponibilidad" value="${ producto.getDisponibilidad()}" 
                    placeholder="${ producto.getDisponibilidad()}" />
                </div>
                <div class="col-12">
                    <input type="text" name="image_url" id="image_url" value=" ${ producto.getImageUrl()}" 
                    placeholder="Imagen" />
                </div>

                <h3 class="col-12">Modelo</h3>
                <div class="col-6 col-12-xsmall">
                    <input type="radio" id="CuelloV" name="modelo" value="Cuello V"  ${ CuelloV} >
                    <label for="CuelloV">Cuello V</label>
                </div>
                <div class="col-6 col-12-xsmall">
                    <input type="radio" id="CuelloRedondo" name="modelo" value="Cuello Redondo" ${ CuelloR} >
                    <label for="CuelloRedondo">Cuello Redondo</label>
                </div>
                
                <h3 class="col-12">Talla</h3>
                <div class="col-4 col-12-small">
                    <input type="checkbox" id="S" name="tallas" value="S"  ${ TallaS} >
                    <label for="S">(S) Chica</label>
                </div>
                <div class="col-4 col-12-small">
                    <input type="checkbox" id="M" name="tallas" value="M"   ${ TallaM} >
                    <label for="M">(M) Mediana</label>
                </div>
                <div class="col-4 col-12-small">
                    <input type="checkbox" id="G" name="tallas" value="G"  ${ TallaG} >
                    <label for="G">(G) Grande</label>
                </div>
                
                
                <h3 class="col-12">Genero</h3>
                <div class="col-6 col-12-small">
                    <input type="radio" id="hombre" name="genero" value="Hombre"  ${ Hombre}>
                    <label for="hombre">Hombre</label>
                </div>
                <div class="col-6 col-12-small">
                    <input type="radio" id="mujer" name="genero" value="Mujer"  ${ Mujer}>
                    <label for="mujer">Mujer</label>
                </div>
               <div class="col-12">
                <textarea type="text" name="descripcion" id="descripcion" placeholder="Descripcion" >${producto.getDescripcion()}
                </textarea>
            </div>

            <div class="col-12">
                <ul class="actions">
                    <li><input type="submit" value="Guardar cambios" class="primary" /> </li>
                </ul>
            </div>
            </div>
        </form>

    </jsp:body>
    </t:dashboard>
           