<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>

    <jsp:attribute name="title">
        <title>Point | Contactanos</title>
    </jsp:attribute>
    <jsp:body>
        <header>
            <h1>Contactanos</h1>
        </header>

        <h4>Ayudanos a mejorar para ti. </h4>
           <p>Si tienes algun comentario o duda, déjanos tus datos y nosotros lo leeremos lo más pronto posible.</p> 
        

        <spring:hasBindErrors name="productoForm">
            <div class="alert alert-danger">
               <ul >
                   <c:forEach items="${errors.allErrors}" var="error">
                        <li>${error.defaultMessage}</li>
                </c:forEach>
               </ul>
            </div>
        </spring:hasBindErrors>

        <c:if test="${successMessage != null && !successMessage.isEmpty()}">
            <div class="alert alert-success ">
                ${successMessage}
            </div>
        </c:if>

        <form method="post" action="/contacto">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <div class="fields">
                <div class="field half">
                    <input type="text" name="nombre" id="nombre" placeholder="Nombre" />
                </div>
                <div class="field half">
                    <input type="text" name="apellido" id="apellido" placeholder="Apellido" />
                </div>
                <div class="field half">
                    <input type="email" name="correo" id="correo" placeholder="Correo" />
                </div>
                <div class="field">
                    <textarea name="comentario" id="comentario" placeholder="¿En qué podemos ayudarte?"></textarea>
                </div>
            </div>
            <ul class="actions">
                <li><input type="submit" value="Listo!! enviar" class="primary" /></li>
            </ul>
        </form>

        <div class="box alt">
            <div class="row gtr-uniform">
                <div class="col-12"><span class="image fit"><img src="/images/contacto.png" alt="" /></span>
                </div></div></div>
  
    </jsp:body>
</t:main>








