<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:main>

    <jsp:attribute name="title">
        <title>Point | Conocenos</title>
    </jsp:attribute>
    <jsp:body>
      
        <section class="row">
            <div class="col-6 col-12-medium">
                <header>
                    <h1>Nuestra Historia</h1>
                </header>
                <p>
                    Somos una empresa pequeña creada para impulsar el desarrollo económico y social de la mujer 
                    mediante actividades de costuras, en las que queremos que dejen una historia detrás de cada 
                    prenda realizada. 
                </p>
                <p>
                    Impulsar la cultura de la textileria en la parte artesanal es nuestra prioridad, 
                    dándole prestigio y sirviendo como plataforma para esas mujeres que quieren empezar 
                    una empresa por si solas. 
                </p>
                <p>
                    Nuestra misión es cumplir con el objetivo establecido de brindar ayuda económica a mujeres.
                </p>
                <p>
                    Nuestra visión es hacernos conocer para qué más mujeres se unan a esta iniciativa y asi, 
                    ayudar a más personas.
                </p>
            </div>
            <div class="col-5"><span class="image fit"><img src="/images/conocenos.png" alt="" /></span></div>
        </section>
    </jsp:body>
</t:main>