<%@ tag description="Dashboard layout" pageEncoding="UTF-8"%>

<%@attribute name="title" fragment="true" %>

<!DOCTYPE HTML>
<!--
    Used and Modify By Daniel Perez for educational use
	Phantom by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<jsp:invoke fragment="title" />
		<meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<link rel="stylesheet" href="/css/main.css" />
		<noscript><link rel="stylesheet" href="/css/noscript.css" /></noscript>
		<link rel="icon" type="image/svg+xml" href="/images/logo.svg" sizes="any">
	</head>


    <body class="is-preload">
		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<div class="inner">

							<!-- Logo -->
								<a href="/admin/dashboard" class="logo">
									<span class="symbol"><img src="/images/logo.svg" /></span><span class="title">PointMty</span>
								</a>
						</div>
					</header>

        <!-- Main -->
					<div id="main">
						<div class="inner">
							
    
         <section>
			<jsp:doBody />
          </section>
        </div>
    </div>
                
    <!-- Footer -->
    <footer id="footer">
        <div class="inner">
            <ul class="copyright">
                <li>&copy; Untitled. All rights reserved</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
            </ul>
        </div>
    </footer>

</div>

    		<!-- Scripts -->
			<script src="/js/jquery.min.js"></script>
			<script src="/js/browser.min.js"></script>
			<script src="/js/breakpoints.min.js"></script>
			<script src="/js/util.js"></script>
			<script src="/js/main.js"></script>

</body>
</html>