<%@ tag description="Main layout" pageEncoding="UTF-8"%>

<%@attribute name="title" fragment="true" %>

<!DOCTYPE HTML>
<!--
	Used and Modify By Daniel Perez for educational use
	Phantom by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<jsp:invoke fragment="title" />
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<link rel="stylesheet" href="/css/main.css" />
		<noscript><link rel="stylesheet" href="/css/noscript.css" /></noscript>
		<link rel="icon" type="image/svg+xml" href="/images/logo.svg" sizes="any">
	</head>
	<body class="is-preload">
		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<div class="inner">

							<!-- Logo -->
								<a href="/" class="logo">
									<span class="symbol"><img src="/images/logo.svg" /></span><span class="title">PointMty</span>
								</a>

							<!-- Nav -->
								<nav>
									<ul>
										<li><a href="#menu">Menu</a></li>
									</ul>
								</nav>

						</div>
					</header>

				<!-- Menu -->
					<nav id="menu">
						<h2>Menu</h2>
						<ul>
							<li><a href="/">Home</a></li>
							<li><a href="/catalogo">Catalogo</a></li>
							<li><a href="/conocenos">Conocenos</a></li>
							<li><a href="/contacto">Contacto</a></li>
						</ul>
					</nav>

				<!-- Main -->
					<div id="main">
						<div class="inner">
                            <section>
                            <jsp:doBody />
                        </section>
                        </div>
					</div>

				<!-- Footer -->
					<footer id="footer">
						<div class="inner">
							
							<section>
								<h2>Siguenos o envianos un mensaje</h2>
								<ul class="icons">
									<li><a href="https://www.instagram.com/pointmty/" class="icon brands style2 fa-instagram"><span class="label">Instagram</span></a></li>
									<li><a href="https://api.whatsapp.com/send?phone=528114754666&text=Hola,%20me%20gustaria%20mas%20informacion" class="icon solid style2 fa-phone"><span class="label">Phone</span></a></li>
									<li><a href="https://bitbucket.org/depm/point/src/master/" class="icon brands style2 fa-github"><span class="label">Repositorio</span></a></li>
								</ul>
							</section>
							<ul class="copyright">
								<li>&copy; Untitled. All rights reserved</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
								<li>Administracion <a href="http://localhost:8080/admin/dashboard">Login</a></li>
							</ul>
						</div>
					</footer>

			</div>

		<!-- Scripts -->
			<script src="/js/jquery.min.js"></script>
			<script src="/js/browser.min.js"></script>
			<script src="/js/breakpoints.min.js"></script>
			<script src="/js/util.js"></script>
			<script src="/js/main.js"></script>

	</body>
</html>