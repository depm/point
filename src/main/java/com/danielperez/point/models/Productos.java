package com.danielperez.point.models;

import javax.persistence.*;

@Entity
@Table(name = "productos")
public class Productos {
  
    @Id
    @Column(name="id",updatable = false, nullable=false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name="nom_pro", nullable = false)
    private String nom_pro;
    
    @Column(name="modelo", nullable = false)
    private String modelo;
    
    @Column(name="tallas", nullable = false)
    private String tallas;
    
    @Column(name="disponibilidad")
    private Integer disponibilidad;
    
    @Column(name="image_url")
    private String image_url;
    
    @Column(name="genero", nullable = false)
    private String genero;

    @Column(name="descripcion", nullable = false)
    private String descripcion;


    public Productos(){}

    public Productos(String nom_pro,
    String modelo,
    String tallas,
    String genero,
    String descripcion){
    this.nom_pro = nom_pro;
    this.modelo = modelo;
    this.tallas = tallas;
    this.genero = genero;
    this.descripcion = descripcion;
    }

    
    public Long getId (){
        return id;
    }

    public String getNomPro() {
        return nom_pro;
    }

    public void setNomPro(String nom_pro) {
        this.nom_pro = nom_pro;
    }

    public String getModelo() {
        return modelo;
    }

    public boolean hasModelo(char modelo) {
        String modelos = getModelo();
        return modelos.indexOf(modelo) >= 0;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTallas() {
        return tallas;
    }

    public boolean hasTallas(char Talla) {
        String tallas = getTallas();
        return tallas.indexOf(Talla) >= 0;
    }

    public void setTallas(String tallas) {
        this.tallas = tallas;
    }

    public Integer getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(Integer disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public String getImageUrl() {
        return image_url;
    }

    public void setImageUrl(String image_url) {
        this.image_url = image_url;
    }

    public String getGenero() {
        return genero;
    }

    public boolean hasGenero(char genero){
        String generos = getGenero();
        return generos.indexOf(genero) >= 0;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}