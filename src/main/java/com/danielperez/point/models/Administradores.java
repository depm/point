package com.danielperez.point.models;

import javax.persistence.*;

@Entity
@Table(name = "administradores")
public class Administradores {

    @Id
    @Column(name="id",updatable = false, nullable=false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="username", nullable=false)
    private String username;
    
    @Column(name="password", nullable=false)
    private String password;

    public Long getId (){
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}