package com.danielperez.point.models;

import javax.persistence.*;

@Entity
@Table(name = "contacto")
public class Contacto {

    
    @Id
    @Column(name="id",updatable = false, nullable=false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    
    @Column(name="nombre", nullable = false)
    private String nombre;
    
    @Column(name="apellido", nullable = false)
    private String apellido;

    @Column(name="correo", nullable = false)
    private String correo;
    
    @Column(name="comentario")
    private String comentario;


    public Contacto(){};
    
    public Contacto(
    String nombre,
    String apellido,
    String correo,
    String comentario){
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.comentario = comentario;
    }

    public Long getId (){
        return id;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getApellido() {
        return apellido;
    }
    
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
        public String getCorreo() {
            return correo;
        }
    
        public void setCorreo(String correo) {
            this.correo = correo;
        }
    
    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
}