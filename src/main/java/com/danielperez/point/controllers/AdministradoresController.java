package com.danielperez.point.controllers;

import java.util.List;

import javax.validation.Valid;

import com.danielperez.point.forms.Productos.StoreProductoForm;
import com.danielperez.point.forms.Productos.UpdateProductoForm;
import com.danielperez.point.models.Contacto;
import com.danielperez.point.models.Productos;
import com.danielperez.point.services.ContactoService;
import com.danielperez.point.services.ProductosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "/admin/dashboard")
public class AdministradoresController {
    
    @Autowired
    ProductosService productoservice;
    
    @Autowired
    ContactoService contactoService;

    @GetMapping(value = "")
    public String index(ModelMap model) {
        List<Productos> productos = productoservice.all();
        model.addAttribute("productos", productos);
        List<Contacto> contacto = contactoService.all();
        model.addAttribute("contacto", contacto);
        return "administradores/index";
    }


    @GetMapping(value = "/crear")
    public String create() {
        
        return "administradores/create";
    }
    @PostMapping(value = "")
    public String store(
        @Valid StoreProductoForm productoForm,
        BindingResult bindingResult,
        RedirectAttributes attributes
    ){

        if (bindingResult.hasErrors()) {
            attributes.addFlashAttribute("org.springframework.validation.BindingResult.productoForm",bindingResult);
            return "redirect:/admin/dashboard/crear";
            
        }
        
        
        Productos producto = new Productos(
            productoForm.getNom_pro(),
            productoForm.getModelo(),
            productoForm.getTallas(),
            productoForm.getGenero(),
            productoForm.getDescripcion()
            );
            
            producto.setDisponibilidad(productoForm.getDisponibilidad());
            producto.setImageUrl("https://dl.dropboxusercontent.com" + productoForm.getImage_url());
            
            productoservice.save(producto);
            
            attributes.addFlashAttribute("successMessage", "Producto creado exitosamente");
        
            return "redirect:/admin/dashboard";
    }
    
    
    @GetMapping(value = "/{productoId}")
    public String edit(@PathVariable Long productoId, Model model) {
        
        Productos producto = productoservice.find(productoId);
        model.addAttribute("producto", producto);
        return "administradores/edit";
    }
    @PostMapping(value = "/{productoId}")
    public String update(
        @PathVariable Long productoId,
        @Valid UpdateProductoForm productoForm,
        BindingResult bindingResult,
        RedirectAttributes attributes
        ){
            
            if (bindingResult.hasErrors()) {
                attributes.addFlashAttribute("org.springframework.validation.BindingResult.productoForm",bindingResult);
            return "redirect:/admin/dashboard/" + productoId;
             }
             
            Productos producto = productoservice.find(productoId);
            
            producto.setNomPro(productoForm.getNom_pro());
            producto.setModelo(productoForm.getModelo());
            producto.setDisponibilidad(productoForm.getDisponibilidad());
            producto.setTallas(productoForm.getTallas());
            producto.setGenero(productoForm.getGenero());
            producto.setDescripcion(productoForm.getDescripcion());
        producto.setImageUrl(productoForm.getImage_url());
        
        productoservice.save(producto);
        
        attributes.addFlashAttribute("successMessage", "Producto actualizado exitosamente");
        return "redirect:/admin/dashboard";
    }

    @PostMapping(value = "/{productoId}/delete")
    public String delete(
        @PathVariable Long productoId,
        RedirectAttributes attributes
    ) {
        Productos producto = productoservice.find(productoId);
        productoservice.delete(producto);
        attributes.addFlashAttribute("successMessage", "El artículo se eliminó correctamente.");
        return "redirect:/admin/dashboard";
    }


}
    
    
    