package com.danielperez.point.controllers;


import javax.validation.Valid;

import com.danielperez.point.forms.contacto.StoreContactosForm;
import com.danielperez.point.models.Contacto;
import com.danielperez.point.services.ContactoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "/contacto")
public class ContactoController {

    @Autowired
    ContactoService contactoService;
    
    @GetMapping(value = "")
    public String contacto() {

        return "contacto/index";
    }

    @PostMapping(value="")
    public String store(
        @Valid StoreContactosForm contactosForm,
        BindingResult bindingResult,
        RedirectAttributes attributes) {
            
            if (bindingResult.hasErrors()) {
            attributes.addFlashAttribute("org.springframework.validation.BindingResult.productoForm",bindingResult);
            return "redirect:/contacto";
                
            };
            
            Contacto contacto = new Contacto(
                contactosForm.getNombre(),
                contactosForm.getApellido(),
                contactosForm.getCorreo(),
                contactosForm.getComentario()
            );
            contactoService.save(contacto);
            
            attributes.addFlashAttribute("successMessage", "Todo bien por aqui!! La informacion ha sido enviada");
            
            return "redirect:/contacto";
        
    }}
        
