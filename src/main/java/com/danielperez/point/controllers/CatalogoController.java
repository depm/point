package com.danielperez.point.controllers;

import java.util.List;

import com.danielperez.point.models.Productos;
import com.danielperez.point.services.ProductosService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
@Controller
@RequestMapping(value = "/catalogo")
public class CatalogoController {
     
    @Autowired
    ProductosService productoservice;

    @GetMapping(value = "")
    public String index(ModelMap model) {
        List<Productos> productos = productoservice.all();
        model.addAttribute("productos", productos);       
        return "catalogo/index";
    }


    @GetMapping(value = "/{productoId}")
    public String show(@PathVariable Long productoId, Model model) {
        
        Productos producto = productoservice.find(productoId);
        model.addAttribute("producto", producto);
        return "catalogo/productoId";
    }}