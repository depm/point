package com.danielperez.point.services;

import java.util.List;

import com.danielperez.point.models.Productos;

public interface ProductosService {
    public List<Productos> all();
    public Productos find (Long id);
    public Boolean save(Productos productos);
    public Boolean delete(Productos productos);
}