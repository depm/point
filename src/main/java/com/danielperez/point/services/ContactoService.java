package com.danielperez.point.services;

import java.util.List;

import com.danielperez.point.models.Contacto;

public interface ContactoService {
    public List<Contacto> all();
    public Contacto find (Long id);
    public Boolean save(Contacto contacto);
}