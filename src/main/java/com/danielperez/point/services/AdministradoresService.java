package com.danielperez.point.services;

import com.danielperez.point.models.Administradores;

public interface AdministradoresService {

    public Administradores find(Long id);
    public Boolean save(Administradores administradores);
    public Administradores findByUsername(String username);
}