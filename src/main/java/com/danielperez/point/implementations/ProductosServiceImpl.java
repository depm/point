package com.danielperez.point.implementations;

import java.util.List;

import com.danielperez.point.models.Productos;
import com.danielperez.point.repositories.ProductosRepository;
import com.danielperez.point.services.ProductosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductosServiceImpl implements ProductosService {

    @Autowired
    private ProductosRepository repository;

    public List<Productos> all() {
        return repository.findAll();
    };

    public Productos find(Long id) {
       return repository.getOne(id);
    }

    public Boolean save(Productos productos) {
        try {
            repository.save(productos);
            return true;
        } catch (Exception e) {
        // Log de la excepcion
        return false;
        }
    }

    public Boolean delete(Productos productos) {
        try {
            repository.delete(productos);
            return true;
        } catch (Exception e) {
        // Log de la excepcion
        return false;
        }
    }

}