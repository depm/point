package com.danielperez.point.implementations;

import com.danielperez.point.models.Administradores;
import com.danielperez.point.repositories.AdministradoresRepository;
import com.danielperez.point.services.AdministradoresService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdministradoresServiceImpl implements AdministradoresService {

    @Autowired
    private AdministradoresRepository repository;

    public Administradores find(Long id){
        return repository.getOne(id);
    }

    public Boolean save(Administradores administradores){
        try {
            repository.save(administradores);
            return true;
        } catch (Exception e) {
        return false;
        }
    }

    public Administradores findByUsername(String username) {
        return repository.findByUsername(username);
    }
}