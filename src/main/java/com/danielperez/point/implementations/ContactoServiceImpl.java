package com.danielperez.point.implementations;

import java.util.List;

import com.danielperez.point.models.Contacto;
import com.danielperez.point.repositories.ContactoRepository;
import com.danielperez.point.services.ContactoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactoServiceImpl implements ContactoService {

    @Autowired
    private ContactoRepository repository;

    public List<Contacto> all() {
        return repository.findAll();
    };

    public Contacto find(Long id){
        return repository.getOne(id);
    }

    public Boolean save(Contacto contacto){
        try {
            repository.save(contacto);
            return true;
        } catch (Exception e) {
        return false;
        }
    }
}