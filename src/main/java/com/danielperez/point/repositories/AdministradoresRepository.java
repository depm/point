package com.danielperez.point.repositories;

import com.danielperez.point.models.Administradores;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministradoresRepository extends JpaRepository<Administradores, Long>{
    public Administradores findByUsername(String username);
}

