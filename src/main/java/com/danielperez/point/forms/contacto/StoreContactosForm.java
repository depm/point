package com.danielperez.point.forms.contacto;

import javax.validation.constraints.NotEmpty;

public class StoreContactosForm {

    
    @NotEmpty(message = "El nombre no puede estar vacio")
    private String nombre;
    
    @NotEmpty(message = "El apellido no puede estar vacio")
    private String apellido;
    
    @NotEmpty(message = "El correo no puede estar vacio")
    private String correo;

    private String comentario;


    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getApellido() {
        return apellido;
    }
    
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getComentario() {
        return comentario;
    }
    
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    @Override
public String toString(){
    return String.format(
    "StoreProductoForm [ nombre=%s,apellido=%s,correo=%s,comentario=%s]",
    nombre,
    apellido,
    correo,
comentario
    );
  
}

}