package com.danielperez.point.forms.Productos;

import javax.validation.constraints.NotEmpty;

public class StoreProductoForm {

@NotEmpty(message = "El nombre del producto no puede estar vacio")
private String nom_pro;

private Integer disponibilidad;

private String image_url;

@NotEmpty(message = "El modelo no puede estar vacio")
private String modelo;

@NotEmpty(message = "La talla no puede estar vacia")
private String tallas;

@NotEmpty(message = "El genero no puede estar vacio")
private String genero;

@NotEmpty(message = "La descripcion no puede estar vacia")
private String descripcion;


public String getNom_pro() {
    return nom_pro;
}

public void setNom_pro(final String nom_pro) {
    this.nom_pro = nom_pro;
}

public Integer getDisponibilidad() {
    return disponibilidad;
}

public void setDisponibilidad(final Integer disponibilidad) {
    this.disponibilidad = disponibilidad;
}

public String getImage_url() {
    return image_url;
}

public void setImage_url(final String image_url) {
    this.image_url = image_url;
}

public String getModelo() {
    return modelo;
}

public void setModelo(final String modelo) {
    this.modelo = modelo;
}

public String getTallas() {
    return tallas;
}

public void setTallas(final String tallas) {
    this.tallas = tallas;
}

public String getGenero() {
    return genero;
}

public void setGenero(final String genero) {
    this.genero = genero;
}

public String getDescripcion() {
    return descripcion;
}

public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
}

@Override
public String toString(){
    return String.format(
    "StoreProductoForm [ nom_pro=%s,disponibilidad=%s,image_url=%s,modelo=%s,tallas=%s,genero=%s,descripcion=%s]",
    nom_pro,
disponibilidad,
image_url,
modelo,
tallas,
genero,
descripcion
    );
  
}

}
